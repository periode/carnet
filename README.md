# carnet

[carnet.enframed.net](http://carnet.enframed.net/)

## ideas

- "i feel like i have my own movie theater" -> apple vision pro and the depoliticization of cinema, the restriction of the screen
- communicative capitalism and name dropping, social capital, moving in and moving out of groups
- change from the top and from the bottom, from inside and outside
- imagining the future
- refusal in public
- considering everyone else
- meaningfulness of actions across time
- the digital space and the physical space
- form (long vs. short, academic vs. casual)
- why does everything have to be "new"?
- private property
- 'the moral barrier to computational complexity' vs. jacques ellul

## misc

- [wholesome knowledge creation](https://stackoverflow.com/questions/46547831/why-my-hugo-theme-host-on-github-page-lost-css-style)
- [sidenotes in hugo](https://danilafe.com/blog/sidenotes/)
