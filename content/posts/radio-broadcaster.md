+++
title = 'Why I like radio (broadcaster edition)'
date = 2024-04-30T18:32:33+02:00
draft = true
summary = "On sharing voices"
+++

## Broadcasting

a field that accompanies you across (stages of) lives

being involved for a while in radio, somewhat, somehow (RCF, RCV, howler, dial)

broadcasting as a way of sharing, and a need to remain aware of what others might be into

Sharing as expression, when you're not sure who's listening, or how, so you're rather caught in the moment.

Sharing as communication, when a particular kind of craft is involved (artists are more freeform, radio hosts rely more on engineers and designers)

Rancière and the aesthetics of politics, occupying space and time differently
