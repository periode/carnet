+++
title = 'Websites vol. 1'
date = 2024-02-09T23:34:26+01:00
draft = false
summary = 'On the best websites lying in my bookmark drawers'
+++

Bookmark folders feel to me like endless drawers in which I put things that I hope that I return to[^0]. Just like real drawers, though, we only get back to it at random moments in our lives[^1][^2]. When I say "return to", it's important to me that we are not the same kind of person who put the thing in storage in the first place. In this sense, I no longer act as creator/archivist, but rather as receiver/public, and if the thing was just tossed in a box with no context, time affects the meaning we can get from it when we retrieve it (picking up a piece of paper in the drawer: "_what was this again?_").

However, this kind of temporalization[^3] can shift if more people are involved: one person (me) can archive and immediately _publish_ it, and another (you) will get to receive it in its meaningful way. Since someone becomes one's own stranger after enough time has passed, presenting work to a (potential) stranger right now gets rid of the need for time to pass by for the meaning of an object to be made explicit. There's probably a thin layer of personal experience that is tied to such object, but that's an acceptable price for sharing.

So here's a first batch of what I found from my bookmark drawers. In case the website is down, you can always try the [wayback machine](https://web.archive.org/)[^4].

- [Brotherʼs car service](https://oliverhelbig.com/party-brotherscarservice/) - It looks like some fancy german graphic designer made a website for the car service of a dude who drove him around ethiopia. I have mixed feelings about digital nomads, but this seems the least disconnected version.
- [Death Row last statements](https://www.tdcj.texas.gov/death_row/dr_executed_offenders.html) - The state of Texas in the U.S.A. publishes the last words of the people they kill.
- [yoo.ooo](http://yoo.ooo/) - Simple but brilliant. When I learned the TLD `.ooo` existed I immediately tried to see if someone seized the occasion, and I was not disappointed.
- [fourmilab.ch](https://fourmilab.ch/) - The website of the guy who co-founded one of the largest and most monopolistic computer-aided design software [autocad](https://www.autodesk.com/products/autocad/overview). He also meticulously documented his life and thoughts in the Swiss mountains.
- [quantum natives](http://quantumnatives.com/) - A lonely person creates whatever world. Each of these tracks have less than a 100 plays.
- [unz](https://web.archive.org/web/20220801041739/https://www.unz.com/) - Good old alternative media, conspiracy fountain in the 2010s.
- [pptArt](https://www.pptart.net/) - Unidentified corporate art object. It seems to be a barely realistic company which organizes its own corporate art fair awarding its own members.
- [sincere](https://www.sincere.chat/) - Last survivor of the golden guest book age. Drop by, have a good time, and sign off!
- [Universal Principles in the Repair of Communication Problems](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0136100) - A study to figure out if "huh?" is the only universal word.
- [astronaut](http://astronaut.io/#) - Watching live YouTube from space. I made something similar for [SoundCloud](https://realtime.enframed.net).
- [photo requests from solitary](https://photorequestsfromsolitary.org/) - A website documenting the kinds of pictures that are requested by prisoners in solitary confinment.
- [unlwd](https://s.ai/nlws/) - The weird world of people who create languages.
- [headgear](https://headgear.pw/-index.html) - The non-linearity of lines and paragraphs.
- [polynesian stick charts](http://thenonist.com/index.php/thenonist/permalink/stick_charts/) - People used to have other (better?) ways of moving around.
- [wonders of street view](https://neal.fun/wonders-of-street-view/) - Speaking of maps.
- [4m3ric4](https://4m3ric4.com/) - Flat rendering of the U.S standard—the road trip.
- [timeline of technologies for publishing](http://recherche.julie-blanc.fr/timeline-publishing/) - Useful framing of how we write mechanically.
- [ExRx](https://exrx.net/) - The most extensive exercises for exercising.
- [ritual engineer](https://ritual.engineer/) - Strong vibes of satanic software engineering.
- [affirmations for my existence](https://tdingsun.github.io/zainab/) - Combinatorial visual poetry.
- [hufeisensiedlung spare parts](http://www.hufeisensiedlung.info/no_cache/Parchimer%20Allee%2081b/fassade/hauseingang.html?houseApart=816&houseDraft=6005&garden=433&location=haus) - An exhaustive documentation of the different parts that inhabitants of the [hufeisensiedlung](https://en.wikipedia.org/wiki/Hufeisensiedlung) might want to refer to. Created by a couple of architects who really like the place.
- [dwitter](https://www.dwitter.net/) - A weird computer graphics community.
- [the ghost in the mp3](https://www.theghostinthemp3.com/theghostinthemp3.html) - MP3 is a lossy compression format. What does the lost data sound like?

[^0]: whose management is not entirely solved ([e.g.](https://github.com/neopostmodern/structure))

[^1]: like a spring cleaning

[^2]: like moving apartments

[^3]: there needs to be some time go by for the archive to settle in, and to make its resurfacing meaningful

[^4]: don't forget to donate <3
