+++
title = 'Setup'
date = 2023-10-05T14:00:25+02:00
summary = 'On practical and sporadic details of the platform'
draft = false
+++

The intent for this blog to exist is mostly __practical__ and __organizational__. Practical in the sense of practicing the writing of thoughts; like everything else, good writing does not fall from the sky, and one's writing only gets better with time and iteration. Organizational, because writing is the organization of one's thoughts (amongst other notational techniques, such as drawing, programming, diagramming). Like writing, it's easy to think, but it's hard to think well.

I spend so much time writing either academic papers or hobbyist software; so I intend for this practical notes to consist of hobbyist papers within a context of academic software[^1]. For now, the whole thing runs with the help of [Hugo](https://gohugo.io/) and autodeploy on [Gitlab](https:/gitlab.com), but like every software, it's never finished, and is always prone to evolve.

Stay tuned,

[^1]: let's see about the bibtex integration though...
