+++
title = 'Life tendencies and facts'
date = 2024-01-01T17:44:23+01:00
draft = false
summary = "On whether regret can be avoided"
+++

## Questioning regret

One summer in the United States, someone we were hanging out with started wondering about regret[^spoiler]. We were a group of friends coming back to our housing from a festival, and we started wondering too. I remember that, as she was trying to figure out with us what is it that constitutes regret, there was no melancholy in her voice. I also remember that the answer wan't that clear.

In particular, we were stuck on whether it was possible to live without regret. We can react as we could have done something for events to turn out differently, but is it really something we have control over? There's a part of responsibility and there's a part of fatality, opposite things which might be falling along the same line which separates the subjective feeling (responsibility) from the objective state (fatality) of affairs. The thing at stake here is the very existence of regret: if it could not have been differently, then _que sera, sera_, as they say, and we can all move on with our lives.

But if it is something that we can affect, if there is at least the feeling of a sort of agency which could have made things turn out differently, then can't we just use this agency to decide that it only amounts to, in the end, an objective state of affairs? Getting rid of regret might be as easy as deciding that there is no agency.

Regret concerns exclusively the things that have already happened, and time only goes one way, so why bother? It's only if you can change things that you can regret not changing them; if there is no solution, then there is no problem. So if we don't have agency, we don't have regret, and if we have agency, we can use it to not have regrets anymore[^lol].

## Individual and social components

I recently found echoes of this whole discussion between particular choice and general constraint in a few different places. Georg Simmel[^simmel], the person who, at the beginning of the 20th century, started thinking about the psychological and social implications of (1) living in big cities and (2) having easy access to money, seemed to consider that this tension is at the core of the thing he calls life. His opinion is that what defines our living is precisely this back and forth between being limited beings, and being able to overcome such limits. Life is always there, and it makes us what we are, but we can also make of life what we want, or at least meaningfully attempt to.

So can we really spin everything in such a way that you have no regrets, transitioning from the subjective to objective, and recognizing that you are only part of this social collective? But only resorting to the relentless force of the collective isn't exactly a way out. The social can also be a source of regrets, because there's a difference when the regrets you have only involve you, or involve others. One can feel regret about decisions that affect just them, and it can be blamed on themselves, on the rest of the world (or on a bit of both). We might regret becaause of others' expectations.

In general, maybe we can think of regret as an emotional response to a past event, as judged by the standards of today. And when we involve standards, we involve comparison, since a standard is only as good as the how different results compare to it, and compare to each other through the standard. Through the other, things immediately take on a strong social tint. Personal decisions can be made in response to social circumstances, or that personal decisions take on a social dimension with negative consequences[^mapped].

So the question of regret is tied to the question of agency and social determinism. No free-will, no regret. If we consider a strong social determinism (i.e. one's actions are largely determined by one's social surroundings), then regret disappears.

## Tendencies and facts

If figuring out the nature of regret involves looking at the tension between individual agency and social constraints, I also found an interesting detour in the development of technology.

When i was reading Yuk Hui[^hui], there was a mention of André Leroi-Gourhan's theory of technological development. Essentially, the question Leroi-Gourhan poses is along the lines of "how come some tools develop in some places and not in others, and how comes some tools seem to develop everywhere?". As an answer, he suggests distinguishing between technical _tendencies_ (which all humans have, such as writing, using their opposable thumbs) and technical _facts_ (which only happens in specific ways in specific cultures, such as writing right-to-left, or top-to-bottom). In short, we're all in the same boat, but we can all row differently.

I think this model actually holds up quite well to what life is, in terms of general dynamics and particular variations[^physics]. If we apply the tendency/fact distinction to the evolution of one's life, then everyone follows the same general pattern, a pattern that is determined by the results of many other individual actions[^birth], but is nonetheless populated with invidual moments that are the results of one's actions.

Within those individual moments, there also seems to be a difference on whether we perceive certain moments as trivial, or as crucial. Crucial moments might be those where we feel the general dynamic could have been altered significantly, and therefore we could have deployed agency for meaningul consequences, ultimately changing the general. One kind of regret could be that we missed such a crucial moment, or that we let so many trivial moments pass by that some kinds of crucial moments will no longer appear. If we alter the general dynamic differently, through actions at a particular moment, it is perhaps in the hope of having less regret in the future. Acting now, so that in the future, there is less regret about the past. This particular stance assumes the significance of individual behaviour to affect the general, but also an interesting conception of time, in which the future can affect the past (at least hopefully).

## Following in style

At some point in my life, I was in a café in [Isfahan (اصفهان)](https://blog.apochi.com/wp-content/uploads/2020/08/Narvan-Cafe-3-1024x677.jpg), when I received two job offers, in two different cities on the opposite sides of the globe[^world]. I thought about it for a while. In the end, I declined both and, a year later, I was in a third city. I often wonder what would have happened if I had taken either and sometimes, regret shows up.

Was that a crucial moment? By changing where I would live for the next foreseeable future, it was definitely going to affect my position in the general dynamic of things. But would I live a fundamentally different kind of life? Or was it always going to follow the same kind of overall shape, or trajectory? As regret amounts to how much we hope we could have changed that overall shape, I'm not sure regret holds up here—I probably would have the same kinds of friends, and the same kinds of jobs. I doubt that my life would have been radically changed.

There's a difference between saying we _could_ have done things differently, and things we _would_ have done things differently. If we couldn't have done things differently, then why resort to wishful thinking? In any case, both are about looking back. If we think about agency differently, then regret disappears, and we cease to be concerned with the past, or with how future actions can lead us to think differently about the past. We just remain in the now. Even if that's a kind of letting go, agency is still involved—still going, but along with the general.

That was the life of [Stoner](https://en.wikipedia.org/wiki/Stoner_(novel)). It was also the life of [Shevek](https://en.wikipedia.org/wiki/The_Dispossessed). You do the things that you are supposed to do, and you try to do them well. From my understanding, this is has echoes of [the way (道)](https://plato.stanford.edu/entries/daoism/#Dao)[^dao]. One is guided along a way, and does well to follow it properly. One still has agency, but it is only aesthetic, insofar sa you can choose _how_ to follow the way. Even if one can still change ways, it is about how you do it, rather than what you do, but you can't change things that have already happened, or have yet to happen. So here, we come back to the present, since neither the past nor future are relevant for our range of action. Being in the present in order to do the coherent (?) gesture in a given situation, as it is happening in _real time_. In order to be adequate to the situation. So it's not whether things could have turned out differently, but how we reacted as they turned out the way they were always going to[^kuolo].

In the end, I think of the japanese archer who says that the correct execution of a movement is being in the right place at the right time[^archer]. By focusing on the way, on its guidance, it can end up being about appreciating doing the least to counter the right place at the right time. The question of what is right might then turn out to be the question of what seems to be the most right in a given present.

So in the end, things happened, happen and will happen. Taking part with good style might be the way to avoid regrets.

[^spoiler]: Spoiler: i'm not sure she has yet to find the answer.

[^simmel]: I read this in the [lebensanschauung](https://www.socio.ch/sim/lebensanschauung/index.htm) (what a webdesign). see also [longue durée](https://en.wikipedia.org/wiki/Longue_dur%C3%A9e) for a more temporal approach to events/structure

[^hui]: [the question regarding technology in china](https://www.urbanomic.com/product/the-question-concerning-technology-in-china/) which was very rich, btw.

[^kuolo]: I wonder if Kuolo Muani regrets. Twenty-something kid who had freshly joined the nationale team and could have become a coutry's hero, missing by a tiny amount an overtime goal in the World Cup finals. Does he regret? Or does he practice?

[^lol]: Or maybe we can still regret not having agency hehe.

[^mapped]: Maybe the first case can map to a regret, and the second one can map to a remorse.

[^physics]: It feels like everything can be reduced to physics.

[^birth]: I guess you can ascribe the time and place of where one is born to one's parents, recursively.

[^world]: But part of the very same world.

[^dao]: If "being" is an essential concept in Western philosophy, then "way" is the core of Chinese philosophy. How much is that a tendency, or a fact?

[^archer]: Can't remember where I heard it, though.
