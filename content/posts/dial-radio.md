+++
title = 'Dial Radio'
date = 2024-06-27T14:38:23+02:00
draft = false
summary = "On building a webradio"
+++

For a bit more than a year, I've been involved in [building a webradio](https://dialradio.live). It broadcasts anyone's selection of music. Over time, it became clear that there were a few interesting things about building such a machine, about giving shape and energy to a kind of infinite jukebox[^jukebox]. It got me thinking about taste, synchronicity, access, privacy, bloat.

At some point, I had to sit down and think through, or clarify, these ideas for a panel presentation at the Lange Nacht der Wissenschaft. You can [read the thoughts here](https://dialradio.live/about).

[^jukebox]: In some ways, it reeks of [industrialization](https://www.cambridge.org/core/journals/popular-music/article/abs/infrastructure-for-the-celestial-jukebox/9DFB681E4C9A10715E7B1ACE6FDAF1E4).
