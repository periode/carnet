+++
title = 'Decision'
date = 2024-06-12T16:42:10+02:00
draft = false
summary = 'On branching choices'
+++

C'est marrant comme il y a des moments auxquels on ne s'attend pas. Des moments auxquels on se retrouve à faire des choix qui nous semblaient invraisemblables il y a (deux semaines|deux mois|deux ans).

Aujourd'hui, je me retrouve à refuser une opportunité de carrière, quelque chose que beaucoup de personnes autour de moi considèrent comme étant le Graal, l'apex de toute une vie d'études.

Sauf que, cette carrière, je ne la partage pas forcément. L'idée de faire de la recherche n'a jamais été quelque chose que j'ai embrassé dès le début de mon master. J'ai toujours préféré être un peu tangeant, à préférer faire de la science politique plutôt que des maths, des jeux vidéos plutôt que des maths, du design plutôt que des jeux vidéos, de l'épistémologie plutôt que du design. J'ai un parcours dans tous les sens, parfois autoroute, parfois sentier de gravier à 13% de pente, parfois le long d'une rivière. Et là, je me retrouve face à une sortie imprévue, une bande de béton qui semble aller tout droit, pendant très, très longtemps. C'est là que la plupart des voyageurs se retrouvent, en file serrée, si ce n'est en embouteillage.

Je cherchais une sortie parce que je me disais bien que le sentier allait finir nulle part; toujours sur une route, certes, mais au revêtement douteux. Et puis, voilà la première sortie. Au début, je pensais que j'allais la rater, que je n'allais pas assez vite, que le saut serait trop haut et trop loin et trop hasardeux. Et puis, en fait, c'est ne pas sauter qui s'avère être risqué. Parce que l'asphalte a l'air jolie, d'ici, mais cette asphalte là n'est pas la seule. Ce qui est risqué, c'est de rester sur le sentier et s'imaginer que le sentier même à une asphalte un peu mieux, une asphalte qui coule sous les roues, qui serpente entre des collines et qui sent bon la pinède. Ce n'est jamais la seule sortie du sentier; et puis, le sentier est parfois mieux que l'asphalte elle-même.

Toujours est-il que c'est dur de _ne pas_ faire ce saut, de laisser passer la sortie, en se disant qu'il y aurait une sortie plus loin. On m'a dit qu'il fallait prendre le job, que _ça ne se refusait pas, une opportunité comme ça_. Cela dit, on ne m'a jamais dit clairement que c'était stupide, qu'il fallait que je remise mes rêves de meilleures sorties au-delà de celle-ci, que j'allais plutôt crever un pneu avant de voir la mer[^pneu].

J'ai l'impression que, dans toutes décisions, il y a toujours différentes facettes d'une personnalité qui se révèlent, qui s'affrontent et se conjuguent. Face à cette sortie, il y a celle qui aime le statut, qui a des folies de grandeur, qui veut _faire quelque chose_ (de grand, de bien, préférablement); il y a celle qui aime faire différemment, qui [ne croit pas à la hype](https://www.youtube.com/watch?v=jfxvYcen4KI&pp=ygUWZG9uJ3QgYmVsaWV2ZSB0aGUgaHlwZQ%3D%3D), qui aime le jeu et le risque plutôt que la certitude. Et puis il y a celle qui doute, celle qui est vulnérable à l'échec, aux refus, et aux regrets[^saut].

Vient alors l'imagination du reste de la route: à quoi est-ce qu'on pensera, lorsque le brouillard tombera, que la conduite deviendra glissante, que l'effort physique supplantera tout plaisir? Quels genres de regrets est-ce qu'on aura? Est-ce qu'on se dira qu'on est _content_ d'être là? Et sur quoi est-ce qu'on s'appuiera pour se persuader de cela? Est-ce qu'on s'appuiera sur un choix individuel, ou est-ce qu'on s'appuiera sur une imposition collective[^collectif]?

Mais plutôt que de théoriser une comparaison de la valeur du choix individuel et du choix collectif, il s'agit plutôt d'être d'accord — _zustimmen_ — avec les conditions de sa vie, quelles qu'en soient les racines: intrinsèques, extrinsèques, hasardeuses et pré-déterminées.

Ce que j'ai fini par choisir, c'est de continuer sur le sentier. À force d'apercevoir des bouts de pins, des effluves d'aiguilles bronzées par le soleil, je me dis que ça vaut le coup de tenter. Je sais que la pinède existe, qu'elle n'est pas si loin, que mon vélo n'est pas le pire pour m'y amener. Et puis, si la sortie que je prends s'avère être un peu moins bien que la première, et bien... c'est le jeu, [comme qui dirait](https://www.youtube.com/watch?v=HTU3L0xZEz0&pp=ygUeYydlc3QgbGUgamV1IG1hIHBhdXZyZSBsdWNldHRl). Et l'important, c'est de participer.

[^pneu]: Un pneu, ça se répare.

[^saut]: Ici, j'ai l'impression que la métaphore du saut vers la sortie ne fonctionne plus si bien. Pour bien sauter, il ne faut plus se distraire, il faut mettre en sourdine toute ces personnalités. Pour faire un choix, un choix éclairé, il faut que le choix soit collégial: qu'il prenne en compte chacune des versions de soi.

[^collectif]: C'est marrant: j'ai parfois l'impression que mes valeurs sociales tendent vers l'imposition collective, mais que mon instinct tend vers une préférence personnelle.
