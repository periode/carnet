+++
title = 'Building ideas'
date = 2023-11-11T22:53:42+01:00
summary = 'On the different implications of making knowledge public'
draft = false
+++

## Making and thinking

As I sit down to write some thoughts on making as a form of knowledge, a very concrete issue shows up: [how do I set up the infrastructure for writing about it](https://duckduckgo.com/?q=hugo+create+new+post&t=newext&atb=v318-1&ia=web)? I thus derail from writing thoughts to searching actions. They feel like unrelated problems but, in some ways, they also feel like they are related.

There is a certain irony of having to figure out how to build a new piece of writing, before writing a piece about building. It feels like getting side-tracked at first, but then I realized I really got caught between two different approaches to making knowledge: fixing a bug and developing programming skills vs. writing some sentences and developing literary skills of thought and inquiry. Dealing (struggling) with a tool might not seem at first like the best way to develop thinking skills but, in the context of programming, the overlap of machine text (code) and human text (word) makes the tool and the thought seem more adjacent. In both ways, we are learning, but could it be that we might be learning the same things, when we are writing programming, and writing about programming?

If there is theoretical thought, and if there is practical thought, they might nonetheless be related in their object (digital writing) and it is only their aproach which differs.

To poke at this question, we can first look at the relationship between form (practice) and content (theory), then at how this relationship evolves as one makes it public, and finally we focus on a concept of _articulation_.

## Directions in content and in form

We can roughly separate the two struggles as belonging to two categories: form, is practice, is code and content, is theory, is prose. The code is that which puts the prose into being. The practice is that which puts the theory into being. These two categories are not opposites, but complementary, and they affect each other in many, subtle ways, ultimately converging into world-existence of a thing[^1].

One of the overlaps is that one can make mistakes in both. That is, undertaking an action which results in something unexpected, often considered an error. Errors are therefore obstacles, because they prevent us from reaching our intended goal in the short-term. And yet, the active encountering of errors is epistemologically stimulating.

On one side, there are some obstacles (such as figuring how to do a well-defined task in a well-defined environment) that I even find fun to do, and part of the fun is that overcoming these errors always involves learning something. In the case of programming, it might just be figuring out how to implement one very-well defined task, which can be assessed in the very short term, such as sorting slices by `time.Time` in Go. In the case of prose, it might be finding the definition of a concept, or finding more details about a topic[^2].

On the other side, errors are always relative (to what one considers correct), and through this relativity the way to the correct solution gets narrowed down, since we dismiss the way that led us to the error (once we can recognize it!).

So we have short-term problems, and immediate solutions, which can be productive.

But then how can we expect these small errors to lead to a larger success? Does looking for immediate solutions to short-term problems bypass the ability to construct a knowledge which would be built on the overcoming of longer-term obstacles? In other terms, are short-term obstacles and long-term obstacles problems of different quantity, or of different quality?

They might point to different directions within a broad, shared field of reference (a programming problem when writing is perceived differently than a cooking problem when painting). One answer to this is to already have a bigger picture of what one wants to be doing, and as such map out the _desired_ direction. Exploratory or discovery approaches do not always guarantee that there is such an overarching plan, and we don't always start with the itinerary to our desired goal (and sometimes not even the goal), so it would be nice if we could derive some macro structure from specific investigations.

At first glance, it seems that long-term knowledge is indeed connected to short-term knowledge—the same word, with different qualifiers. An obvious case is in gestalt psychology, which argues that the whole becomes bigger than the sum of its parts, and yet such whole does not exist without parts. What's the nature of the relation between these two aspects, then? In the specific case of visual reasoning it seems to be that spatial arrangement of parts is crucial. In other cases it might be that the sequential order of things are important, like in a story, or in film editing.

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/2/27/Reification.svg" title="Reification" caption="Finding wholes from parts" >}}

Architecture has similar interrogations about the detail and the structure. There are a lot of different ways we could consider the detail, but I like to consider it as a reliable testimony of the fact that the person who made this detail _cared_. It can be a good indicator that someone has been dutifully involved in the process of construction and/or design, and one could therefore assume with reasonable chance that the same care is given to the rest of the structure. A nice detail is a good hint of a nice whole, and it's definitely easier (at least for [some people](https://en.wikipedia.org/wiki/Ren%C3%A9_Descartes)) to fully grasp a series of details than the whole.

That's from a spectator's perspective. From a constructor's perspective, I like the concept of _[piecemeal growth](http://akkartik.name/post/habitability)_. It hints at the becoming of a structure, and does not require such a structure to be pre-existing, but rather only a structured to be extended. This is about the process of building, thinking about both a chair and wood, about a story and syntax, about a document and code. Building ideas might not be such a different process, if we were to think of ideas as a material. Perhaps the most rough idea-material are synaptic connection and, since they're also the most fleeting, they require materialization. And ideas materialize as:

- speech
- movement
- writing
- drawing
- sculpting
- ...

So ideas can be built out of all mediums, as short configurations of matter can be reassembled in a receiver's mind and reveal (1) intent and (2) wordview. Short-term problems (how do I fix Y or Z) and long-term problems (how do I formulate A or B) are therefore idea generating at the subjective level, since they involve the navigation of the idea-space.

The problem takes on another dimension, or quality, once we need to go beyond just exploring that idea, but to communicate it. At this point, presentation is no longer a medium for thought but a medium for humans; it's not about exploration and form-finding (convergence into something crisp), it's about restitution (making sure that nothing gets lost in translation).

## Doing so publicly

Doing something publically immediately implies the possibility for self-awareness: one can think in public (lost in thought in the subway) or one can make the thinking public (write something intended for an other). When doing making thinking public, we project the thought in certain configurations, and the standard of success of these configurations is the extent to which the resulting idea is being faithfully understood (as per [Claude](https://en.wikipedia.org/wiki/Shannon%E2%80%93Weaver_model)), or productively understood (as per [Roman](https://en.wikipedia.org/wiki/Jakobson%27s_functions_of_language)). As we become in another's thought processes, it's hard to negate the altruistic component of making public [^3].

Hence the presentation of ideas: not only is it a question of visibility, but also of _legibility_, as an organization of form which conveys the content. Before, as we were still forming our thoughts, the form was just about pinning down these fleeting ideas, of given them shape which could further be recombined into ideas. Words into sentences into paragraphs into opinions.

Public making, if it is to be useful, also involves public presentation, and I think it's important to separate presentation-as-expression and presentation-as-communication, with the former relying on emotional impression, and the latter on cognitive involvement. Public expression is the mediated restitution of one's internal thought processes, or the result of the internal thought processes, with a focus on the processes themselves, by virtue of their [autotelic](https://en.wiktionary.org/wiki/autotelic) nature. In this configuration, the presentation of the idea can be discussed by the recipient, but it's a lot harder to discuss the idea itself. Since the idea is its own justification, it is also its own gold standard, and thus cannot be judged relative to something else: in which case, one is forcing the expression into the communication realm, and such translation might see something get lost in the process.

Presentation-as-communication is therefore something else. It is a way of presenting an idea in such a way that the core aspects of the idea (e.g. its statement, its origin, its relations, its implications) can in turn become building blocks of subsequent idea-finding. The communication process should allow for the content of the communication to be subsumed, either by being adapted, modified, or recombined with other ideas in order to create new ones. This does not mean that the recombination of expressed ideas cannot take place, but I would tend to think that it often consists in the recombination of the forms of its presentation, in a kind of [remix](https://lessig.org/product/remix/).

This is a conception of presentation as a base for communication as a prelude to creation. It is a sort of _shared attention_, of meaning-making as a common activity this time, and no longer a personal one.

The thing is, just because this presentation involves a cognitive function (communicating an idea as substrate for other ideas), it does not imply that there is only one way to have this presentation laid out. Coming back the mention of altruism mentioned earlier/above[^4].

Style is how form becomes public, either highlighting [the group or the individual](https://journals.sagepub.com/doi/10.1177/026327691008003004). As an individual, it is about recognizing _the source_ of the idea, the person behind it, and this person has full liberty of choosing their own style, notwithstanding whether they decide to correlate with other individuals' style, hence leaning into social style. As a group, it is about recognizing _the destination_ of the idea, the persons who will use them. The choice of which style to use becomes, paradoxically, more rigid. A [Ted talk](https://www.youtube.com/watch?v=ePRiHbM1Ngo), an academic presentation, a heated discussion at the bar, a debate in a classroom, a conflict at a counter, all of these communicational decisions involve a particular style of interaction, from the most explicit (the Ted talk) to the most implicit (how the person at the counter is best receptive to ideas).

Still, one can attempt to master different styles in order to understand the implications of style itself (e.g. structural arrangement, aesthetic evokation, genetic evolution). This dabbling in the variety of style not only reveals the versatility of formal approaches and the skills necessary to best match those forms to the ideas that are to be communicated, but is also a counterpoint to the dangerous illusion that a specific manner is enough to cover the breadth and variety of these original synaptic connections.

Because of the self-conscious part of becoming public, of the awareness that is is the self that is consciously being and communicating, it thus becomes unavoidable to consider that there is value in paying attention to how one presents one's ideas, if one wants to be recognized[^5]. It also impacts how seriously one's contribution to the discourse[^6] is being taken.

## The articulation

The textual sketches above are pointing at different kinds of relation. A relation between small problems and big problems. A relation between content and form. A relation between an individual and the public, as opposed to the relationn between an individual and their idea(s).

The concept of relation is a bit [too broad](https://link.springer.com/article/10.1007/s10502-021-09357-0) to be dealt with in the conclusion here, so I'd rather focus on a similar, yet different, one—_articulation_.

I consider articulation to be the operational, as opposed to a more theoretical relation. That is, it takes places in concrete situations and can change configurations in specific ways, while relations I consider as more of a potential. A group has relations, a team has articulations.

Thinking about how both form and content, as well as public and private, articulate, involves thinking about how everything is involved and contribute to communicating the super-structure, the implicit backdrop that connects them all through pre-existing semantics, consequently establish one or more new semantics. Meanings create articulation, which create meaning in return.

To do so, we need to find the parts that are most prone to connecting to other syntactic entities. Based on the feature of a given entity, some might overlap, depend on or point to[^7] another entity. That might be considered a short, immediate articulation. A longer, more temporal articulation might be one which is present, through contingent features, in the background of other entities (a word might be connected to an immediate sentence, and contribute to the tone of a paragraph).

Finally, this articulation is dynamic, and therefore should support action. An articulation can move, and in this movement lead the articulated entities into motion. In this sense, it starts from a concretized relation (a part of a sentence, or of a drawing), and thus highlights a subspace of other potential relations. You can twist them into something which changes, but nonetheless remains coherent. Since people like to rearrange things[^8], to make (body or thought) familiar and comfortable in their given environment, this might be a part of how we can build on things, and develop meaning.

[^1]: that sounds like a stereotypical german philosophy sentence.

[^2]: granted, finding something in prose tends to be a lot more fuzzy.

[^3]: doing publically also modifies the temporality of the thought, as making public increases an idea's odds of survival.

[^4]: funny how space and time can be equated. i think it was bergson who said "we're looking for time but all we find is space".

[^5]: the assumption here is that everyone wants to be recognized.

[^6]: _la perception d'un individu comme étant force de proposition_.

[^7]: insert other (spatially?) transitive verb here?

[^8]: see in particular the handling of free standing furniture in [the social life of small urban spaces](https://archive.org/details/sociallifeofsmal0000whyt/page/n5/mode/2up).
