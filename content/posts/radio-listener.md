+++
title = 'Why I like radio (listener edition)'
date = 2024-03-22T00:14:26+01:00
draft = false
summary = "On social listening"
+++

I like radio.

Originally, the term _radio_ is a strictly technical one. It only refers to the part of what we know as radio[^we] involving electromagnetic frequencies of the radiowave spectrum. That alone is a kind of magic, in which we oscillate the ether to carry whispers through. But what I refer to as radio is another kind of magic, one in which chance, desire, community and liveness play major parts.

There are many reasons why I like radio, but I think they gravitate around the fact that it's a social technology, that it creates community. It is a space both for the many (mass media) and for the dual (speaker and listener), but never for the single. There's always a broadcaster, and always a listener. Radio is never a lonely affair.

## Serendipity

Turning the dial across frequencies is like dipping in and out of conversations, listening sessions, concerts, readings, or shopping centers (thanks, ads). It's like entering rooms where you know you're never disturbing, you're always welcome, and you never know what will come next[^dial].

I remember fiddling with the volume knob late at night to keep sound of a forbidden program at its lowest, because there was school the day after and mom would take the receiver away. Making sure that the exchange between the DJ and me would remain secret.

Stumbling upon a broadcast on the anniversary of the fall of the wall and frantically writing down in a notebook all the [songs](https://www.youtube.com/watch?v=i8JBUktSxvQ) that were played. A treasure trove of music unheard of, like a waterfall.

I particularly remember that, in Abu Dhabi, there was a very specific range where one could tune into a broadcast which would only play synth-instrumental versions of vintage pop songs (think _Total Eclipse of the Heart_, without lyrics, played on a cheap Casio keyboard). It was always a delight when I managed to catch it, and would let it fill the night, even though I have no idea what it is called, or what is the frequency. So even though radio is everywhere, radio is also localized, and this particular broadcast exists only on this section of the northern shore of the arabian peninsula. And there are also places where radio never comes through (parking lots), or places where the [radio only spans a housing block](https://anarchy.translocal.jp/radio/micro/what_is_minifm/index.html)!

It never stops and it's everywhere, but we only ever listen to it here and now (the thing is _live_). Any particular moment of a particular song has a tint of intimacy, of something special of being there now, rather than at a different place or a different time. It's a perfect staging for serendipity[^serendipity]. Even though you're always grabbing the stream at different times, and it happens that it's the perfect timing.

## Live listening

And since it never stops, it also accompanies you throughout the day, the season, or throughout life. Each moment of the day has a show, each season sees its changes[^season], and goodbyes can get emotional[^end]. Seen this way, it creates a rhythm, a tapestry that sometimes frames moments of the day, or of the week, on which we can rely on. The most popular shows on the usual timeslots (e.g. early morning, lunch break) are called _meetings_, where we gather with one another to spend some time together, __sharing our attention__. Same place, same time, same distant listeners. And even though listening is always subjective, attention is social: you exchange in a group, usually with the other person in the room or in the car, or with roommates, or with loved ones. I particularly like how it seems to naturally tend to recede in the background, unless it's so loud that it imposes itself, and leave all possible space to the listeners, to hum along, to react, to comment, to riff off from.

Thinking of it, this touches on the difference between a radio show and a podcast. A podcast inverses this relationship to time, in which the podcast is ever-available, while the radio show is ever-fleeting. I think we lost a specific kind of pleasure with podcasts, and their on-demand nature, like a different feeling when hearing the music theme of a particular show, hearing it come in, rather than putting it on.

## Unstoppable frequencies

It's a constant stream which can be made audible or made silent, but remains always present. It took me a while to be able to grasp that part: electromagnetic waves are everywhere, all around us, all the time. The radio (the object, the receiver) is only there to catch those waves, which transform into electricity (similar, but different waves) and then into sound (yet another wave). The receiver is a catalyzer, it brings in something else into the now. The radio always vibing, and it's up to us to tune in.

There's also something interesting about the technical infrastructure. You can't stop radio—at least the FM sort. It goes across borders[^borders], across fields and buildings, and operates according to its own logic when it comes to fading in or fading out. The only way to disrupt radio is to disrupt the waves, so you don't turn off a single radio, you turn off the whole radio waves. Software-defined radio is slightly similar, insofar as no one can prevent you from broadcasting, except perhaps by seizing your domain name, but domain names are cheap.

Maybe it's because of this unstoppable nature that pirate radios have such an appeal to me. They're always there, always in the cracks and breaches of authorized, institutional media. They're the counterpoint to Billboard 500/Top 40 repeats, the weird crackles that take you by surprise, that welcome you into a small, ephemeral group which, nonetheless, remains alive, for the time being.

<!-- I also like [broadcasting](./radio-broadcaster.md). -->

[^we]: songs, discussions, receivers, car rides and background music

[^dial]: this is something we lost in digital receivers which scan directly to the next registered frequency. Such a system feels a little bit closer to television.

[^serendipity]: I remember a couple of occurences like this. Like this one time, when I was talking to my brother about a [reggae cover of take my breath away](https://www.youtube.com/watch?v=EaiXLkp8E2k), as we got in his car and turned it on, that song was playing. Or feeling down during the day when, at night, slipped in-between two up-beat neo-jazz tracks, [the interlude from my favorite album](https://www.youtube.com/watch?v=ahbjxQCGwTE&list=OLAK5uy_mSOrabW7sZvU-9OGyBtxSEGxiH6Ub2s9w&index=7) comes to hang in my living room.

[^end]: Biking through the city in summer listening to [the last installment of my favorite program](https://www.radiofrance.fr/franceculture/podcasts/les-chemins-de-la-philosophie/les-chemins-de-la-philosophie-du-vendredi-01-juillet-2022-5882705), dripping tears of slightly bitter joy quickly dried in the warm wind.

[^season]: Listening to a weekly show, on a day when the show host could not join, and [the sub earned himself a permanent slot](https://www.nts.live/shows/breakfast/episodes/the-nts-breakfast-show-w-scratcha-dva-21st-october-2021) out of sheer energy.

[^borders]: In recent developments in East Africa, the change of foreign interference in the balance of power (roughly, from French to Russian), and the subsequent digital censorship measures, led foreign governments to re-invest in analog broadcasting to keep sharing their point of view.
