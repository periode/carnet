+++
title = 'Renovating a personal page'
date = 2024-02-09T05:05:07+01:00
draft = false
summary = "On maintaining and sustaining the publicly personal"
+++

It's a funny feeling to renovate [a personal website](https://pierredepaz.net) one has not touched in years.

I wrote the first version of this website on a spring day of 2015. I was called for jury duty, so I showed up to the courtroom for the day but, in the end, I was not picked to be part of those who decide about justice. However, since I did not have an internet connection on my laptop, I killed the time by making my website (90% of the visual design was decided on that day).

Thanks to the SEO gods, it it has steady, if ephemeral visitors. The strange thing about online visitors is that you cannot see them, except through an analytics dashboard. So, on one side, it feels like a somewhat professional display, as people here about me, google me, and end up on the landing page. On the other side, it feels like a diary in public, as I used it to convey quirky messages, love letters and various toy projects I made throughout the years.

The motivation for renovation stemmed from the impression that it was getting outdated, and was definitely bloated. The original version had a custom NodeJS script to serve all assets, and was doing template rendering in PugJS[^1]. Coming back from a [conference](https://undonecs.sciencesconf.org/) where we talked a lot about the limits to digital technologies, this setup absolutely felt like it was very wasteful. Is it then ironic that I switched one cobbling of frameworks for another framework (svelte)? It might be more efficient in the current version (building static HTML files), but unfortunately, it's also less readable since all the javascript gets heavily minified/uglified. The compromise to this loss of inspectability made me think that I could made the source code directly available on a repo, rather than relying on the files to be self-explanatory. But having a public repo brought more questions, of a different nature.

Renovating this website, this ambivalence between personal and public (rather than private, since private sounds to me like more of a display mechanism rather than an emotional load, at least within digital environments). Indeed, framing it as a personal website inadvertently ushers in the question of who I am as a person, and whether it's possible to represent it all at once.

Using the word _renovating_ also allows me to bypass the question of _cleaning_, which sounds a little too discriminatory, implying there are things that are dirty and need to be gotten rid of. It is setting an implicit standard, veering slightly towards a more LinkedIn version of this page. Particularly for [projects](http://pierredepaz.net/past), I was wondering how to draw a distinction between those that are of a certain significance, and those that are not. I thought about featured/normal, serious/toy, main/sub, until I settled on the __ongoing/archive__, as the temporal dimension is something we can never escape, and that everyone has an understanding of.

It definitely made me feel a bit nostalgic for a while (perhaps it was also because I was doing it at night, alone, conditions through feelings flow best). Browsing through both projects and structure seemed like sifting through past lives, through possibilities and realizations, ideas drafted and technologies tried out, a weird kind of maintainer's [anamnesis](https://en.wikipedia.org/wiki/Anamnesis_(philosophy)).

I wonder what is the other side's perception.

[^1]: a somewhat cute reminder of my technical skills at the time
